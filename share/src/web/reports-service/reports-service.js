var RPT = RPT || {};

(function() {
	var DEFAULT_VALUE = "default";
	
	var Dom = YAHOO.util.Dom;
	var Event = YAHOO.util.Event;
	var $html = Alfresco.util.encodeHTML;

	RPT.ReportsService = function TabularReport(htmlId) {

		return RPT.ReportsService.superclass.constructor.call(this,"RPT.ReportsService", htmlId);
	};

	YAHOO.extend(RPT.ReportsService, Alfresco.component.Base, {
		onReady : function ReportService_onReady() {
			var $this = this;
			
			//Preparing iframe
			this.widgets.iframeWrapper = Dom.get(this.id + "-iframeWrapper");
			this.widgets.iframe = Dom.get(this.id + "-iframe");
			this.widgets.iframeTitle = Dom.get(this.id + "-iframe-title");
			this.widgets.webviewURI = Alfresco.constants.PROXY_URI + "com/yellresearch/rpt/reportsService";
			this.widgets.webviewTitle = "Reports";
			this.widgets.iframe.src = this.widgets.webviewURI;
			this.widgets.iframeTitle.style.cursor = "pointer";
			this.widgets.iframeTitle.innerHTML = $html(this.widgets.webviewTitle);

			var params = {
				scope : $this, 
				webviewURI : this.widgets.webviewURI
			};

			Event.on(this.widgets.iframeTitle, "click", $this.onIframeTitleClick, params);
			
			//Adding iframe
			Dom.addClass(this.id, "webview-iframe");

		},
		
		//Save selected report and bind it to href
		onIframeTitleClick : function onIframeTitleClick(e, params) {
			var $this = params.scope;
			var webviewURI = params.webviewURI;

			// get tableId
			var datatableSelector = $this.widgets.iframe.contentDocument.querySelector("#datatable-selector");
			if (datatableSelector) {
				var selectedOptValue = datatableSelector.selectedOptions[0].value;
			}
			
			//Open to new window
			window.open(webviewURI + "?tableId=" + selectedOptValue, "_blank");

		}
	});
})();