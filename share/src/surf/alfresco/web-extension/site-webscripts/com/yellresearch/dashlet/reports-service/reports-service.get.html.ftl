<#assign el=args.htmlid?html>

<@markup id="css" >
	<#-- CSS Dependencies -->
	<@link rel="stylesheet" type="text/css" href="${url.context}/res/reports-service/reports-service.css"  group="dashlets" />
</@>

<@markup id="js">
	<#-- JavaScript Dependencies -->
	<@script type="text/javascript" src="${url.context}/res/reports-service/reports-service.js" group="dashlets"/>
</@>

<@markup id="widgets">
	<#assign id=el?replace("-", "_")>
	<@createWidgets group="dashlets"/>
</@>

<@markup id="html">
	<div class="dashlet webview">
	<div class="title">
		<a id="${args.htmlid}-iframe-title" class="iframe-title" target="_blank" title="Open to new window"></a>
	</div>
		<div class="body scrollablePanel"<#if args.height??> style="height: ${args.height}px;"</#if> id="${args.htmlid}-iframeWrapper">
			<iframe id="${args.htmlid}-iframe" class="iframe-body" frameborder="0" scrolling="auto" width="100%" height="100%"></iframe>
			<div class="resize-mask"></div>
		</div>
	</div>
</@>

