function main(){
	
	var reportsService = {
		id: "reportsService",
		name: "RPT.ReportsService"
	};

	var dashletResizer = {
		id: "DashletResizer",
		name: "Alfresco.widget.DashletResizer",
		initArgs: ["\"" + args.htmlid + "\"", "\"" + instance.object.id + "\""],
		useMessages: false
	};

	model.widgets = [reportsService, dashletResizer];
}
main();