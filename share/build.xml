<?xml version="1.0"?>

<!-- ======================================================================= -->
<!-- Alfresco build environment                                              -->
<!-- ======================================================================= -->

<!-- PLEASE CHANGE THIS PROJECT NAME! -->
<project name="Reports - Share" default="build" basedir=".">

    <import file="minify.xml" />
    
    <!-- include ant-contrib.jar -->
	<taskdef resource="net/sf/antcontrib/antlib.xml">
		<classpath>
			<pathelement location="./../lib/ant-contrib-1.0b3.jar" />
		</classpath>
	</taskdef>
    
	<!-- bring through environment variables -->
	<property environment="env"/>

	<!-- read the properties file to override any of these defaults -->
	<property file="build.properties"/>

	<!-- detect what type of formatter to use -->
	<condition property="junit.formatter" value="xml" else="plain">
		<equals arg1="${env.BUILD_ENVIRONMENT}" arg2="ci" />
	</condition>

	<!-- see if the project has the SURF libraries locally, otherwise set a value and indicate to copy them across -->
	<available file="${runtime.dir}/WEB-INF/web.xml" type="file" property="surf.build.exists" />

	<available file="${basedir}/custom.properties" type="file" property="custom.properties.exists" />

	<fail unless="custom.properties.exists" message="Please create a custom.properties file in ${basedir} (a sample file is available in that directory)." />

	<!-- the custom configuration properties if set. These are read below and overridden if the values are supplied by the command line instead -->
	<property file="custom.properties"/>

	<path id="all-classpath">
		<fileset dir="${lib.dir}">
			<include name="**/*.jar" />
		</fileset>
		<fileset dir="${runtime.lib}">
			<include name="**/*.jar" />
		</fileset>
	</path>

	<target name="init">
		<tstamp>
			<format property="TODAY" pattern="d-MM-yy" />
		</tstamp>
	</target>

	<target name="copy-surf" unless="surf.build.exists">
		<echo>SURF libraries and configuration not found at ${runtime.dir}, copying from ${surf.location}</echo>
		<mkdir dir="${runtime.dir}" />
		<copy todir="${runtime.dir}">
			<fileset dir="${surf.location}">
				<include name="**/*" />
			</fileset>
		</copy>
	</target>

	<!-- =================================================================== -->
	<!-- Compiles all classes and creates a jar file -->
	<!-- =================================================================== -->
	<target name="compile-classes" depends="init">
		<mkdir dir="${build.webinf.dir}" />
		<mkdir dir="${build.classes.dir}" />

		<javac destdir="${build.classes.dir}" debug="${build.debug}" deprecation="off" optimize="${build.optimize}" source="${build.java_version}" target="${build.java_version}" includes="**/*.java">
			<src path="${src.java}" />
			<classpath refid="all-classpath" />
		</javac>
	</target>

	<target name="copy-web-resources">
		<copy todir="${build.dir}">
			<fileset dir="${src.web}">
				<include name="**/*" />
				<exclude name="**/*.svn" />
			</fileset>
		</copy>
	</target>


	<target name="copy-surf-resources">
		<copy todir="${build.classes.dir}">
			<fileset dir="${src.surf}">
				<include name="**/*" />
				<exclude name="**/*.svn" />
			</fileset>
		</copy>
	</target>

	<target name="copy-lib">
		<copy todir="${build.lib.dir}">
			<fileset dir="${lib.dir}">
				<include name="**/*" />
				<exclude name="servlet-api.jar"/>
				<exclude name="**/*.svn" />
			</fileset>
		</copy>
	</target>

	<!-- deploys to the local runtime 'deployed' directory which is used to actually run the application -->
	<target name="deploy-local">
		<copy todir="${runtime.dir}">
			<fileset dir="${build.dir}">
				<include name="**/*" />
				<exclude name="**/*.svn" />
			</fileset>
		</copy>
	</target>

	<target name="copy-config">
		<copy todir="${build.classes.dir}">
			<fileset dir="${config.webapp}">
				<include name="**/*" />
				<exclude name="**/*.svn" />
				<exclude name="**/log4j.properties" />
			</fileset>			
		</copy>

	</target>

	<target name="build-base" depends="init,copy-surf,compile-classes,copy-lib,copy-config,copy-web-resources,copy-surf-resources,deploy-local">
	</target>

	<target name="build"
		depends="build-base,dupe-min-files,copy-files-to-build-deployed">
	</target>
	
	<target name="create-jar" depends="build-base,minify-web-resources,copy-files-to-build-deployed">
		<echo>Creating jar ${jar.file}</echo>
		<mkdir dir="${build.lib.dir}" />
		<mkdir dir="${build.dir}/META-INF" />
		<jar destfile="${jar.file}">
			<metainf dir="${build.dir}/META-INF" />
			<fileset dir="${build.classes.dir}" excludes="**/*Test*" includes="**/*.class" />
			<fileset dir="${src.java}" excludes="**/*.java" includes="**/*" />
			<!-- CI-5 CUSTOMIZATION - Add webscripts to jar -->
			<fileset dir="${src.surf}" includes="**/*" />
			<!-- CI-5 CUSTOMIZATION - Add client scripts to META-INF folder in order to display the UI -->
			<metainf dir="${src.web}" />
		</jar>
		<delete dir="${build.dir}/META-INF" />
	</target>

	<!-- Builds the module -->
	<target name="module" depends="clean,create-jar">
		<delete file="${amp.file}" />

		<property name="module.web.dir" value="${build.module}/web" />
		<property name="module.config.dir" value="${build.module}/config" />
        <property name="module.webinf.dir" value="${build.module}/WEB-INF/classes" />
        <property name="module.webinf.lib.dir" value="${build.module}/WEB-INF/lib" />
        <mkdir dir="${module.web.dir}" />
	    <mkdir dir="${module.config.dir}" />
        <mkdir dir="${module.webinf.dir}" />
        <mkdir dir="${module.webinf.lib.dir}" />

        <!-- copy any libraries -->
        <echo>Copying libraries</echo>
		<copy todir="${module.webinf.lib.dir}" file="${jar.file}"/>
        <copy todir="${module.webinf.lib.dir}">
            <fileset dir="${lib.dir}">
                <include name="**/*" />
                <exclude name="servlet-api.jar"/>
                <exclude name="**/*.svn" />
            </fileset>  
        </copy>

		<!-- Copy Surf Configurations -->
		<copy todir="${module.webinf.dir}">
			<fileset dir="${src.surf}">
				<include name="**/*" />
				<exclude name="**/*.svn" />
			</fileset>
		</copy>

        <!-- copy any web folders too -->
        <copy todir="${module.web.dir}">
            <fileset dir="${src.web}">
                <include name="**/*" />
                <exclude name="**/*.svn" />
                <exclude name="**/WEB-INF*" />
            </fileset>
        </copy>

		<!-- copy stuff for the module's configuration dirs -->
		<copy todir="${module.config.dir}">
			<fileset dir="${config.webapp}">
				<include name="**/*" />
				<exclude name="**/*.svn" />
				<exclude name="**/module.properties" />
				<exclude name="**/file-mapping.properties" />
				<exclude name="**/log4j.properties" />
			</fileset>
		</copy>

        <copy todir="${build.module}" file="${config.webapp}/alfresco/module/${module.name}/module.properties" />
        <copy todir="${build.module}" file="${config.webapp}/alfresco/module/${module.name}/file-mapping.properties" />

		<zip destfile="${amp.file}" >
			<fileset dir="${build.module}">
				<include name="**/*" />
			</fileset>
		</zip>
	</target>

	<!-- =================================================================== -->
	<!-- Testing related targets                                             -->
	<!-- =================================================================== -->
	<target name="build-tests" depends="compile-classes">
		<mkdir dir="${src.test-resources.classes.dir}" />

		<javac destdir="${src.test-resources.classes.dir}" debug="on" deprecation="off" optimize="off" source="1.5" includes="**/*.java">
			<src path="${src.test-resources.java.dir}" />
			<classpath refid="test-classpath" />
		</javac>
	</target>

	<target name="execute-tests">
		<mkdir dir="${src.test-resources.dir}" />
		<mkdir dir="${src.test-resources.dir}/results" />
		<junit printsummary="yes" fork="yes" maxmemory="128M" haltonfailure="no" dir="${src.test-resources.dir}">
			<!--<jvmarg value="-server"/>-->
			<classpath refid="test-classpath" />
			<formatter type="${junit.formatter}" />
			<batchtest todir="${src.test-resources.dir}/results">
				<fileset dir="${src.test-resources.java.dir}">
					<include name="com/**/Test*.java" />
				</fileset>
			</batchtest>
		</junit>
	</target>

	<target name="run-tests" depends="build-tests,execute-tests" description="Runs the projects unit tests">
	</target>

	<target name="war" depends="clean-all,compile-classes,copy-lib,copy-web-resources,copy-surf-resources,deploy-local">
		<!-- zip everything into a war file -->
		<war destfile="${war.file}" needxmlfile="false">
			<fileset dir="${runtime.dir}">
			</fileset>
		</war>
	</target>

	<path id="test-classpath">
		<fileset dir="${lib.dir}">
			<include name="**/*.jar" />
		</fileset>
		<fileset dir="${runtime.lib}">
			<include name="**/*.jar" />
		</fileset>
		<pathelement path="${runtime.classes}" />
		<pathelement path="${src.test-resources.classes.dir}" />
	</path>

	<!-- =================================================================== -->
	<!-- Explains how to use this build file                                 -->
	<!-- =================================================================== -->
	<target name="help" depends="init">
		<echo>

 USEFUL TARGETS
 ===============
 The following useful targets are available (the default is compile).

 clean           - cleans intermediate build products (does not remove built items from the deployed directory)
 build           - performs a complete compile of the environment and deploys the files into the /deployed dir to be run by the Eclipse-tomcat plugin
 war			 - Creates a WAR file containing Alfresco AND any modifications made by this project
 run-tests       - Runs all tests

        </echo>
	</target>

	<!-- =================================================================== -->
	<!-- Cleans out the project                                              -->
	<!-- =================================================================== -->
	<target name="clean" >
		<echo>Cleaning the build folders</echo>
		<delete dir="${build.dir}" />
	</target>

	<target name="clean-all" >
		<echo>Cleaning all project assets</echo>
		<delete dir="${build.dir}" />
		<delete dir="${runtime.dir}" />

	</target>
	
	<target name="rsync-local-share">
			<exec executable="rsync" dir="${basedir}/deployed">
				<arg line="-av --chmod=a+rwx --delete --exclude .git . ${locald.user.name}@${locald.host}:${locald.location.share}" />
			</exec>
		</target>
	
	<target name="rsync-remote-share" >
		<exec executable="rsync" dir="${basedir}/deployed">
			<arg line="-av --chmod=a+rwx --delete --exclude .git . ${docker.user.name}@${docker.host}:${docker.location.share}" />
		</exec>
	</target>
	
</project>
