package com.yellresearch.nga.POI;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.alfresco.model.ContentModel;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.model.FileNotFoundException;
import org.alfresco.service.cmr.repository.ContentIOException;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.ContentService;
import org.alfresco.service.cmr.security.PersonService;
import org.alfresco.repo.jscript.BaseScopableProcessorExtension;
import org.apache.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;



public class xlsxUtils extends BaseScopableProcessorExtension  {
	/**
	 * DMCNGA-883 Tabular Report Dashlet - Add feature "Export to XLSX"
	 * DMCNGA-885 Tabular Report Dashlet - Create xlsxUtils class
	 * - Adds function to write data to a specific sheet of a xlsx file, copy the template and save it to a destination folder. 
	 * - Adds function to read an xlsx file and return the data as json string
	 */
	
	private ContentService contentService;
	private NodeService nodeService;
	private InputStream in;
	private OutputStream out;
	private PersonService personService;
	private ServiceRegistry serviceRegistry;
	private static final Logger log = Logger.getLogger(xlsxUtils.class);

	//CI-4 CUSTOMIZATION - Add columnsArray parameter
	public void write(String templateNodeRef, String sheetName, String destinationFolder, String jsonData, String[] columnsArray) throws ContentIOException, IOException, JSONException, FileNotFoundException {
		
		FileFolderService fileFolderService = serviceRegistry.getFileFolderService();
		
		try {
							
			NodeRef tempNodeRef = new NodeRef(templateNodeRef);
			NodeRef destNodeRef = new NodeRef(destinationFolder);
			JSONArray dataArray = new JSONArray(jsonData);
			
			in = contentService.getReader(tempNodeRef, ContentModel.PROP_CONTENT).getContentInputStream();
			
			//Copy template to destination folder
			String templateName = (String) nodeService.getProperty(tempNodeRef, ContentModel.PROP_NAME);
			FileInfo newFile = fileFolderService.copy(tempNodeRef, destNodeRef, templateName);
			NodeRef newFileNodeRef = newFile.getNodeRef();
			out = contentService.getWriter(newFileNodeRef, ContentModel.PROP_CONTENT, true).getContentOutputStream();
		
			//Get workbook
			XSSFWorkbook wb = new XSSFWorkbook(in);	
			XSSFSheet sheet;
			
			//Check if sheet exist in template workbook, create if no
			if (isSheetExist(sheetName, wb)) {
				sheet = wb.getSheet(sheetName);
			} else {
				sheet = wb.createSheet(sheetName);
			}
			
			//Populating xlsx header row 
			Row headerRow = sheet.createRow(0); //Header row
			int cIndex = 0;
			
			// CI-4 CUSTOMIZATION Start - Populate table fields using columnsArray 
			for (int i = 0, l = columnsArray.length; i < l; i++) {
				Cell cell = headerRow.createCell(cIndex);	//Create cell
				cell.setCellType(Cell.CELL_TYPE_STRING); 	//Set cell type
				cell.setCellValue((String)columnsArray[i]);		//Insert value
				cIndex++;
			}
			// CI-4 CUSTOMIZATION End
			
			//Populating data into template
			int rIndex = 1; //Initial row pointer
			
			for (int x = 0; x < dataArray.length(); x++) {
				
				JSONObject objRow = dataArray.getJSONObject(x);
				Row row = sheet.createRow(rIndex);
				cIndex = 0;
				
				//Populate row's cells
				// CI-4 CUSTOMIZATION Start - Populate value using columnsArray
				for (int i = 0; i < columnsArray.length; i++) {
					Cell cell = row.createCell(cIndex);
					cell.setCellType(Cell.CELL_TYPE_STRING);
					
					String value = objRow.getString((String)columnsArray[i]);
					cell.setCellValue(value);
					cIndex++;
				}
				// CI-4 CUSTOMIZATION End
				
				rIndex++;
			}
			
			wb.write(out);
			in.close();
			out.close();
			
		} catch (Exception e) {
			log.error("Error found in xlsxUtils. Unable write to workbook", e);
			
		} finally {
			if (in != null) {
				in.close();
			}
			
			if (out != null ) {
				out.close();
			}	
		}
	}
	
	//Return data from sheet as json
	public String getData(String fileNodeRef, String sheetName) throws ContentIOException, IOException, JSONException {
		
		NodeRef xlsRef = new NodeRef(fileNodeRef);
		
		//Get xlsx 
		XSSFWorkbook wb = new XSSFWorkbook(contentService.getReader(xlsRef, ContentModel.PROP_CONTENT).getContentInputStream());
		
		//Get sheet
		XSSFSheet sheet = wb.getSheet(sheetName);
		
		//Create json container
		Collection<JSONObject> data = new ArrayList<JSONObject>();
		
		//Iterate through the rows
		for (Iterator<Row> rowsIt = sheet.rowIterator(); rowsIt.hasNext();) {

			Row row = rowsIt.next();
			Row headerRow = sheet.getRow(0);
			
			//Skips header
			if (row.getRowNum() != 0) {
				
				//Iterate through the cells 
				JSONObject cells = new JSONObject();
			
				Iterator<Cell> c = row.cellIterator();
				while (c.hasNext()) {
					Cell cell = c.next();
					cell.setCellType(Cell.CELL_TYPE_STRING);
					// headerRow.getCell(cell.getColumnIndex()) is the key, cell.getStringCellValue is the value
					cells.put(headerRow.getCell(cell.getColumnIndex()).toString(), cell.getStringCellValue());
				}
				
				//Pushes values of each row into container
				data.add(cells);
			}
		}
		
		//Return data as JSON string
		return data.toString();
	}
	
	//Function to check if sheet exist within workbook
	public boolean isSheetExist(String sheetName, XSSFWorkbook wb) {
		
		if (wb.getSheetIndex(sheetName.toUpperCase()) == -1 || wb.getSheetIndex(sheetName) == -1) {
			return false;
		} else {
			return true;
		}		
	}

	public ContentService getContentService() {
		return contentService;
	}

	public void setContentService(ContentService contentService) {
		this.contentService = contentService;
	}
	
	public ServiceRegistry getServiceRegistry() {
		return serviceRegistry;
	}

	public void setServiceRegistry(ServiceRegistry serviceRegistry) {
		this.serviceRegistry = serviceRegistry;
	}
	
	public NodeService getNodeService() {
		return nodeService;
	}

	public void setNodeService(NodeService nodeService) {
		this.nodeService = nodeService;
	}
	
	public PersonService getPersonService() {
		return personService;
	}

	public void setPersonService(PersonService personService) {
		this.personService = personService;
	}
}