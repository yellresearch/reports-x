function main(){
	/**
	 * DMCNGA-778 Retrieve data for Tabular Reports dashlet
	 * DMCNGA-883 Add feature "Export to XLSX template" 
	 */
	
	//Paths
	var TEMPLATE_FOLDER = companyhome.childByNamePath("Data Dictionary/Lateral Minds/Reports/XLSX Templates");
	var DEFINITION_FOLDER = companyhome.childByNamePath("Data Dictionary/Lateral Minds/Reports/Definitions");
	var queryList = getList(DEFINITION_FOLDER);
		
	if (!queryList) { 
		//Default list of SQL Statements, if SQL list is not found
		var queryList = [{
			//Default option
			id: "default",		//Unique ID 
			group: "",			//Report grouping 
			title: "Select:",	//Title to be displayed on menu
			sqlQuery: null,		//SQL query to execute 
			inputParameters: {
				type: "",
				target: ""
			}
		}, {
			//Test SQL query
			id: "report1",
			group: "Group1",
			title: "Title of report 1",
			xlsxTemplates : [ "Test.xlsx", "Test-Not Created.xlsx" ],
			sqlQuery: [{ 
							name: "sql11", 
							sql: "select * from alf_qname limit 10",
							options : []
						} , {
							name: "sql11", 
							sql: "select * from alf_qname where local_name='$qname$'",
							options : ["override"]
						}], 	
			inputParameters: {
				type: "subSelect",
				target: "$qname$",
				subSelectQuery: "select distinct local_name from alf_qname limit 10",
				options: []
			}
		}, {
			//Test SQL query
			id: "report3",
			group: "Group2",
			title: "Title of report 3",
			xlsxTemplates : [ "Test.xlsx", "Test-Not Created.xlsx" ],
			sqlQuery: [{ 
							name: "sql11", 
							sql: "SELECT id,version,change_txn_id,commit_time_ms FROM alf_transaction limit 10",
							options : []
						} , {
							name: "sql11", 
							sql: "SELECT id,version,change_txn_id,commit_time_ms FROM alf_transaction WHERE id = '$id$'",
							options : ["override"]
						}], 	
			inputParameters: {
				type: "searchString",
				target: "$id$"
			}

		}];
	}
	
	if (args["format"]) {
		var format = args["format"];
	}
	
	if (args["tableId"]) {
		var tableId = args["tableId"];
	}
	
	if (args["exportFormat"]) {
		var exportFormat = args["exportFormat"];
	}
	
	// CI-6 CUSTOMIZATION Start - Get selected template name
	if (args["selectedTemplate"]) {
		var selectedTemplate = args["selectedTemplate"];
	}
	// CI-6 CUSTOMIZATION End
	
	//Get subSelect list
	for each(var list in queryList) {
		var subSelectionOpt = [];
		if (list.inputParameters.type == "subSelect" && list.inputParameters.subSelectQuery) {
			try {
				var query = list.inputParameters.subSelectQuery;
				var options = dbUtils.query(query);
				for (var i = 0, l = options.length; i < l; i++){
					option = {
							name: options[i][0],
							id: "opt" + i
					}
					list.inputParameters.options.push(option);
				}
				
			} catch (e) {
				throw "Error retrieving subSelection options due to: " + e;
			}
		}
	}
	
	//DMCNGA-780 CUSTOMIZATION - Export Result as CSV 
	if (tableId && format == "csv") {
		var queryList = filterQueryList(queryList, tableId);		
	} 
	//DMCNGA-780 CUSTOMIZATION - End
	
	//DMCNGA-883 CUSTOMIZATION - Export data into a xlsx template and generate download link
	if (tableId && exportFormat == "xlsx"){
		try {
			var templates = TEMPLATE_FOLDER.childFileFolders();
		} catch(e) {		
			throw "Error when retrieving child file folder from [" + TEMPLATE_FOLDER.name + "]: " + e;
		}
		
		//Check for existing destination folder at userhome; create if no. 
		var destinationFolder = "Reports";
		var dFolder = userhome.childByNamePath(destinationFolder);
		
		if (dFolder == null && userhome.hasPermission("CreateChildren")){
			dFolder = userhome.createFolder(destinationFolder);
		}
		
		//Check if template exist from list of templates defined in definition
		var templateFound = false;
		for (i = 0; i < templates.length; i++) {
			// CI-6 CUSTOMIZATION - change variable to 'selectedTemplate'
			if (templates[i].name == selectedTemplate){
				
				templateFound = true;
				
				var filteredList = getData(filterQueryList(queryList, tableId));
				var filteredListObj = filteredList[0];
				var filteredListData = filteredListObj.data[0];
				var data = jsonUtils.toJSONString(filteredListData.content);
				var columnsArray = filteredListData.columnsArray;
				var templateNode = templates[i].nodeRef.toString();
				var sheet = "Data";
				dFolderNodeRef = dFolder.nodeRef.toString();
				
				//Remove xlsx file in destination folder, if exist
				var xlsxFile = userhome.childByNamePath(destinationFolder + "/" + templates[i].name);
				
				if (xlsxFile != null){
					xlsxFile.remove();
				}
				
				//Write data to template
				//CI-4 CUSTOMIZATION - Add columnsArray parameter
				xlsxUtils.write(templateNode, sheet, dFolderNodeRef, data, columnsArray);
				
				//Generating download link 
				xlsxFile = userhome.childByNamePath(destinationFolder + "/" + templates[i].name);
				var url = "/share/proxy/alfresco/api/node/content/workspace/SpacesStore/" + xlsxFile.id + "/" + xlsxFile.name;
				model.xlsxUrl = url;

				break;
			} 
		}
		if (!templateFound) {
			model.templateInvalid = "true";
		}
	}
	//DMCNGA-883 CUSTOMIZATION - End
	
	var results = getData(queryList);
	
	return results;
}

//Get definitions
function getList (definitionFolder) {
	
	var list = [];
	var SUFFIX = ".definition";
	var DEF_ID_PREFIX = "rpt";
	
	try {
		var definitions = definitionFolder.childFileFolders();
	} catch(e) {		
		throw "Error when retrieving child file folder from [" + definitionFolder.name + "]: " + e;
	}
	
	for (var i = 0, l = definitions.length; i < l; i++){
		var definition = definitions[i];
		var filename = definition.name;
		
		//Grab file with suffix ".definition"
		if (filename.substr(filename.indexOf('.')) == SUFFIX){
			try {
				var obj = eval("(" + definition.content + ")");
			} catch(e) {		
				return null;
			}
			
			obj.id = DEF_ID_PREFIX + i;
			list.push(obj);
		}
	}
	
	if (list.length == 0) {
		return null;
	}
	
	return list;
}

//Filter out unrequested queries from queryList
function filterQueryList(queryList, queryId) {
	
	var newQueryList = []; 
	
	for (var i = 0, l = queryList.length; i < l; i++) {
		
		var queryInfo = queryList[i];
		
		if (queryInfo.id == queryId) {
			newQueryList.push(queryInfo);
			break;
		}	
	}
	return newQueryList;
}

//Get data from database
function getData(queryList) {
	
	if (args["startDate"]) {
		var startDate = args["startDate"];
	}
	
	if (args["endDate"]) {
		var endDate = args["endDate"];
	}
	
	if (args["tableId"]) {
		var tableId = args["tableId"];
	}
	
	if (args["subSelect"]) {
		var subSelection = args["subSelect"];
	}
	
	if (args["searchQuery"]) {
		var searchQuery = args["searchQuery"];
	}
	
	for (var i = 0, l = queryList.length; i < l; i++) {
		
		var data = null;
		var queryInfo = queryList[i];
		queryInfo.data = [];
		
		//DMCNGA-904 CUSTOMIZATION - Queries selected report only
		if (queryInfo.sqlQuery && tableId == queryInfo.id) {
		//DMCNGA-904 CUSTOMIZATION - end
			
			//DMCNGA-812 CUSTOMIZATION - Add option for date range. Checks for date input parameters
			if (queryInfo.inputParameters.type == "dateRange" && startDate && endDate && tableId == queryInfo.id) {
				
				var target = queryInfo.inputParameters.target;
				var sorting = queryInfo.inputParameters.sorting;
				
				//Check if contains override sql
				for (var x = 0, y = queryInfo.sqlQuery.length; x < y; x++) {
					
					//If true, replace default SQL with override SQL and additional date params
					if (queryInfo.sqlQuery[x].options.indexOf("override") > -1) {
						
						var newQuery =	queryInfo.sqlQuery[x].sql.replace("$startDate$", startDate);
						newQuery = newQuery.replace("$endDate$", endDate);
						break; 
					
					//else, add date params to default SQL
					} else {
						var newQuery =	queryInfo.sqlQuery[0].sql.replace("$startDate$", startDate);
						newQuery = newQuery.replace("$endDate$", endDate);
					}
				}
										
				queryInfo.sqlQuery[0].sql = newQuery;
						
			}			
			//DMCNGA-812 CUSTOMIZATION - End
			
			//DMCNGA-954 CUSTOMIZATION - Add option for date range. Checks for date input parameters
			if (queryInfo.inputParameters.type == "subSelect" && subSelection && tableId == queryInfo.id) {
				
				var target = queryInfo.inputParameters.target;
				
				var options = queryInfo.inputParameters.options;
				
				for each (var option in options) {
					if (option.id == subSelection) {
						subSelection = option.name;
						break;
					}
				}
				
				var newQuery =	queryInfo.sqlQuery[1].sql.replace(target, subSelection);
				queryInfo.sqlQuery[0].sql = newQuery;
					
			
			}
			//DMCNGA-954 CUSTOMIZATION - End
			
			//DMCNGA-937 CUSTOMIZATION - Add option for search string
			if (queryInfo.inputParameters.type == "searchString" && searchQuery && tableId == queryInfo.id) {
				
				var target = queryInfo.inputParameters.target;
				
				//Check if contains override sql
				for (var x = 0, y = queryInfo.sqlQuery.length; x < y; x++) {
					
					//If true, replace default SQL with override SQL and replace string
					if (queryInfo.sqlQuery[x].options.indexOf("override") > -1) {
						
						var newQuery =	queryInfo.sqlQuery[x].sql.replace(target, searchQuery);
						break; 
					
					//else, replace string on default SQL
					} else {
						var newQuery =	queryInfo.sqlQuery[0].sql.replace(target, searchQuery);
					}
				}
										
				queryInfo.sqlQuery[0].sql = newQuery;
						
			}			
			
			
			//DMCNGA-937 CUSTOMIZATION - End 
			
			for (var x = 0, y = queryInfo.sqlQuery.length; x < y; x++) {
				
				//DMCNGA-812 CUSTOMIZATION - Skip SQLs with option "override"
				if (!(queryInfo.sqlQuery[x].options.indexOf("override") > -1)) {
				//DMCNGA-812 CUSTOMIZATION - End
					
					//DMCNGA-954 CUSTOMIZATION - Extra check. If input param is "subSelect", only query for index 0
					if (!(queryInfo.inputParameters.type=="subSelect" && x>0)) {
					//DMCNGA-954 CUSTOMIZATION - End	
						
						try {
							data = dbUtils.query(queryInfo.sqlQuery[x].sql);
							
							//CI-4 CUSTOMIZATION Start - get array of keys from data[0], and put to obj.columnsArray
							var colOrder = [];
							var rowData = data[0];
							for (var key in rowData) {
								colOrder.push(key);
							}
							//CI-4 CUSTOMIZATION End
						} catch(e) {		
							var sqlQuery = queryInfo.sqlQuery[x].sql;
							throw "Error when executing " + queryInfo.id + ". " + sqlQuery + ". Unable to retrieve data due to: " + e;
						}
						
						//Push data
						var obj = {
									name: queryInfo.sqlQuery[x].name,
									options: queryInfo.sqlQuery[x].options,
									content: data,
									//CI-4 CUSTOMIZATION - store array of table columns
									columnsArray: colOrder
								};
						
						queryInfo.data.push(obj);
						
					}
				}
			}	
		} else {
			queryInfo.data = data;
		}				 	
	}	
	return queryList;
}

model.results = main();