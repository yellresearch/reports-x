<#assign result = results[0]>
<#list result.data as data>
<#if data.content??>
<#assign cols = data.content[0]?keys>
<#list cols as col>${col}<#if col_has_next>,</#if></#list> 
<#list data.content as row>
<#assign keys = row?keys>
<#list keys as key><#if row[key]??>${row[key]}</#if><#if key_has_next>,</#if></#list>
</#list>
</#if>
</#list>