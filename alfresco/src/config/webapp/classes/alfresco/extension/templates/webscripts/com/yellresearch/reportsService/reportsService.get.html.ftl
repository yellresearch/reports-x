<style>
div.toolbar {
	position: fixed;
	width: 100%;
	top: 0px;
	padding: 8px;
	border-bottom: 1px dotted #BCBCBC;
	background:#eee;
	z-index:10;
	font-size: 10pt;
	font-family: open sans, arial;
	color: #333333;
}

body {
	margin : 0px;
}

div.dateToolbar, .submitButton, .rightToolbar, .searchToolbar, .subSelectToolbar, .datatable-selector {
	display: none;
}

div.submitButton {
	padding-top:5px;
}

div.leftToolbar {
	float: left; 
}

div.rightToolbar {
	float: right;
	border-left: 1px dotted #BCBCBC;
	padding: 0px 0px 10px 10px;
}

div.datatable-container {
	display: none;
	padding-top: 30px;
	z-index: 5;
	overflow: auto;
}

div.export-xlsx {
	display: none;
}

select {
	height: 22px;
}

select.leftToolbar {
	width: 255px;
}

select.export {
	width: 155px;
}

form { 
	display: table;
	margin-bottom: 3px;
	width: 98%;
}

option:first-child {
    color: #ccc;
}

p.dateToolbar {
	display: table-row;
}

label {
	padding-right: 7px;
	text-align: right;
}

label.leftToolbar, input {
	display: table-cell;
	
}

table.datatable {
	margin: 20px;
	color: #777777;
}

th.datatableData {
	color: #2D2D2D;
	background-color: #D8D8DA;
	font-size: large;
}

table.datatable, th.datatableData, td.datatableData {
	font-family: open sans, arial;
	font-size: 9pt;
	border: 1px solid #BCBCBC;
	border-collapse: collapse;
}

th.datatableData, td.datatableData {
	padding: 5px 10px 5px 10px;
	vertical-align: middle;
}

/* CI-4 CUSTOMIZATION - UI alignment, create class for label to set the width. */
.leftToolbarLabel {
	display: inline-block;
	text-align: left;
	width: 50px;
}

.leftToolbarLabelDate {
	display: inline-block;
	text-align: left;
	width: 70px;
}
</style>
<body>
<div class="toolbar">
	<form>
		<div class="leftToolbar">
			<!--  L-TOOLBAR: Report Selector -->
			<label class="leftToolbarLabel">${msg("reports-service.selector-label")} :</label>
			<select class="leftToolbar" id="datatable-selector" name="tableId">
				<option selected disabled>Select :</option>
				<#list results as result>
				<option value="${result.id}">
					${result.title}
				</option>
				</#list>
			</select>
			
			<#list results as result>
			<#if result.inputParameters.type = "subSelect">
			
			<!--  L-TOOLBAR: Subselect -->
			<div class="subSelectToolbar" id="${result.id}">
				<#if result.inputParameters.subSelectTitle??>
					<label class="leftToolbarLabel">${result.inputParameters.subSelectTitle} :</label>
				</#if>
			
				<select class="leftToolbar" name="subSelect">
					<#if result.inputParameters.options?? && result.inputParameters.options[0]??>
						<option selected disabled>${msg("reports-service.label.select")} :</option>
						<#list result.inputParameters.options as option>
						<option value="${option.id}">
							${option.name}
						</option>
						</#list>
					<#else>
						<option selected disabled>-- ${msg("reports-service.label.no-options")} --</option>
					</#if>	
				</select>
			</div>
			</#if>
			</#list>
			
			<!--  L-TOOLBAR: Date -->
			<div class="dateToolbar">	
				<p class="dateToolbar">
					<label class="leftToolbarLabelDate">${msg("reports-service.start-date-label")} : </label> 
					<input type="date" id="startDate" name="startDate"></input>		
				</p>
				
				<p class="dateToolbar">
					<label class="leftToolbarLabelDate">${msg("reports-service.end-date-label")}  : </label>
					<input type="date" id="endDate" name="endDate"></input>
				</p>
			</div>
			
			<!-- L-TOOLBAR: Submit -->
			<div class="submitButton" align="right">
				<button type="submit" name="submitButton" value="run" style="width:68px">${msg("reports-service.run-button")}</button>
			</div>
		</div>
		
		<div class="rightToolbar"> 
			<div style="border-bottom:1px dotted #BCBCBC; text-align:right; padding-bottom:2px">
				${msg("reports-service.right-toolbar-label")}
			</div>
			<div style="padding-top:6px">
				<!-- R-TOOLBAR: Export to CSV -->
				<!-- Remove Export to CSV option  -->
				<div class="export-csv" align="right" style="display:none"> 
				<!-- Remove Export to CSV option  -->	
					<label>${msg("reports-service.csv-export-label")} :</label>
					<select class="export" name="template">
						<option selected disabled>${msg("reports-service.csv-export-initial")}</option>
						<option>CSV</option>	
					</select>
					<button type="submit" name="format" value="csv" style="width:35px" formtarget="_blank">${msg("reports-service.csv-export-button")}</button>
				</div>
				
				<!-- CI-6 CUSTOMIZATION Start - New hidden input field to capture the selected xlsx template name -->
				<input type="hidden" id="selectedTemplate" name="selectedTemplate">
				<!-- CI-6 CUSTOMIZATION End -->
				
				<!-- R-TOOLBAR: Export to XLSX -->
				<#list results as result>
				<div class="export-xlsx" id="${result.id}" align="right">
					<#if result.xlsxTemplates?? && (result.xlsxTemplates?size != 0)>
						<label>${msg("reports-service.xlsx-export-label")} :</label>
						<!-- CI-6 CUSTOMIZATION Start - Add onchange for dropdown menu -->
						<select class="export" name="template" onchange="onChangeExportXlsx()">
						<!-- CI-6 CUSTOMIZATION End -->
							<#list result.xlsxTemplates as template>
								<option value="${template}">
									${template}
								</option>
							</#list>
						</select>
						<button type="submit" name="exportFormat" value="xlsx" style="width:35px">${msg("reports-service.xlsx-export-button")}</button>
						<div style="text-align:right"> 
							<#if templateInvalid?? && templateInvalid=="true">
								<font color="red">${msg("reports-service.template-not-found")}</font>
							</#if>
						</div>
					<#else>	
						<label>${msg("reports-service.xlsx-export-label")} :</label>
						<select class="export" name="template">
							<option selected disabled>N/A</option>>
						</select>
					</#if>
				</div>
				</#list>
				<#if xlsxUrl??>
					<div id="xlsxUrl" data-xlsxUrl="${xlsxUrl}" style="display:none"></div>
				</#if>
				
				
			</div>
		</div>
	</form>
</div>

<#list results as result>
<#if result.inputParameters.type??>
	<#assign inputType = result.inputParameters.type>
<#else> 
	<#assign inputType = "">
</#if>
<div id="${result.id}" class="datatable-container" data-input-type="${inputType}" >
	
	<#if result.data??>
	<#list result.data as data>
		<#if data.content?? && data.content[0]??>
		<table class="datatable">
			<tr class="datatableData">
				<#-- CI-4 CUSTOMIZATION Start - Get the array of columns -->
				<#assign cols = data.columnsArray>
				<#-- CI-4 CUSTOMIZATION End -->
				
				<#if data.options?? && !data.options?seq_contains("hideHeader")>				
					<#list cols as col>
						<th class="datatableData">${col}</th>
					</#list>				
				</#if>
			</tr> 
			<#list data.content as row>
			<tr class="datatableData">
				<#-- CI-4 CUSTOMIZATION Start - Get the array of columns -->
				<#assign keys = data.columnsArray>
				<#-- CI-4 CUSTOMIZATION End -->
				
				<#list keys as key>
					<td class="datatableData">
						<#if row[key]??>
							${row[key]}
						</#if>
					</td>
				</#list>
			</tr>
			</#list>
		</table>
		<#else>
			<div style="margin: 20px">
				${msg("reports-service.no-result")}
			</div>
		</#if>
	</#list>

	</#if>
</div>
</#list>
</body>

<script>
var datatableSelectorEl = document.querySelector("#datatable-selector");
var submitButton = document.querySelector(".submitButton");

var urlParams;

//Extract URL query if any		
(window.onpopstate = function() {
	var match;
	var symbol = /\+/g; //Regex to check for symbol and replace with space
	var search = /([^&=]+)=?([^&]*)/g;
	var decode = function(s) { 
		return decodeURIComponent(s.replace(symbol, " ")); 
	};
	var query  = window.location.search.substring(1);

	urlParams = {};
	while (match = search.exec(query)) {
		urlParams[decode(match[1])] = decode(match[2]);
	}
	
	var tables = document.querySelectorAll(".datatable-container");
	
	//Set padding of each table		
	for (var i = 0, l = tables.length; i < l; i++) {
	
		var table = tables[i];
		var paramType = table.getAttribute("data-input-type");
		
		if (paramType == "dateRange" || paramType == "subSelect") {
			table.style.padding = "98px 8px"
		} else {
			table.style.padding = "60px 8px"
		}	
	}	
	
	//Set default values and load table
	if (urlParams["tableId"]) {
		document.getElementById("datatable-selector").value = urlParams["tableId"];
		var xlsxToolbars = document.querySelectorAll(".export-xlsx")
		var selectedTableId = urlParams["tableId"];
		var dateToolbar = document.querySelector(".dateToolbar");
		var subSelToolbars = document.querySelectorAll(".subSelectToolbar");
		var rightToolbar = document.querySelector(".rightToolbar");
		
		rightToolbar.style.display = "block";
		submitButton.style.display = "block";
		dateToolbar.style.display = "none";
				
		for (var i = 0, l = tables.length; i < l; i++) {
			var table = tables[i];
			var paramType = table.getAttribute("data-input-type");
			
			table.style.display = "none";

			if (table.id == selectedTableId) {
				table.style.display = "block";
				
				if (paramType == "dateRange") {
					dateToolbar.style.display = "block";
				} 
			}
		}
		
		for (var i = 0, l = xlsxToolbars.length; i < l; i++) {
		var xlsxToolbar = xlsxToolbars[i];
		
		xlsxToolbar.style.display = "none";
		
			if (xlsxToolbar.id == selectedTableId) {
				xlsxToolbar.style.display = "block";
	
			}
		}
		
		//Display subselect input 
		for (var x = 0, len = subSelToolbars.length; x < len; x++) {
			
			var subSelToolbar = subSelToolbars[x];
			subSelToolbar.style.display = "none";
			
			if (subSelToolbar.id == selectedTableId) {
				
				if (urlParams["subSelect"]) {
					var selectEl = subSelToolbar.getElementsByTagName("select");
					selectEl[0].value = urlParams["subSelect"];
				}
				
				subSelToolbar.style.display = "block";
			}
		}
	}
	
	if (urlParams["startDate"]) {
		document.getElementById("startDate").defaultValue = urlParams["startDate"];
	}
	
	if (urlParams["endDate"]) {
		document.getElementById("endDate").defaultValue = urlParams["endDate"];
	}	
	
	//Download ready xlsxTemplate, if exist
	var xlsxURL = document.getElementById("xlsxUrl");
	
	if (xlsxURL) {
		var url = xlsxURL.getAttribute("data-xlsxUrl");
		window.location.href = url;
	}
	
	//CI-6 CUSTOMIZATION - Set template name when page load
	setSelectedTemplate();
	//CI-6 CUSTOMIZATION End
})();


datatableSelectorEl.onchange = function onChangeDatatable(e) {
	var tables = document.querySelectorAll(".datatable-container");
	var xlsxToolbars = document.querySelectorAll(".export-xlsx")
	var selectedTableId = datatableSelectorEl.selectedOptions[0].value;
	var dateToolbar = document.querySelector(".dateToolbar");
	var subSelToolbars = document.querySelectorAll(".subSelectToolbar");
	
	var rightToolbar = document.querySelector(".rightToolbar");
	rightToolbar.style.display = "block";
	dateToolbar.style.display = "none";
	submitButton.style.display = "block";
			
	for (var i = 0, l = tables.length; i < l; i++) {
		var table = tables[i];
		var paramType = table.getAttribute("data-input-type");
				
		table.style.display = "none";
		
		if (table.id == selectedTableId) {
			table.style.display = "block";
	
			if (paramType == "dateRange") {
				dateToolbar.style.display = "block";
			}
		}
	}
	
	for (var i = 0, l = xlsxToolbars.length; i < l; i++) {
		var xlsxToolbar = xlsxToolbars[i];
		
		xlsxToolbar.style.display = "none";
		
		if (xlsxToolbar.id == selectedTableId) {
			xlsxToolbar.style.display = "block";

		}
	}
	
	//Display subselect input 
	for (var x = 0, len = subSelToolbars.length; x < len; x++) {
		
		var subSelToolbar = subSelToolbars[x];
		subSelToolbar.style.display = "none";
		
		if (subSelToolbar.id == selectedTableId) {
			
			if (urlParams["subSelect"]) {
				var selectEl = subSelToolbar.getElementsByTagName("select");
				selectEl[0].value = urlParams["subSelect"];
			}
			
			subSelToolbar.style.display = "block";
		}
	}
	
	//CI-6 CUSTOMIZATION - Set template name when onchange report dropdown list
	setSelectedTemplate();
	//CI-6 CUSTOMIZATION End
} 

//CI-6 CUSTOMIZATION - Add onchange function for export xlsx dropdown menu
function onChangeExportXlsx() {
	
	//Set template name when onchange export xlsx dropdown list
	setSelectedTemplate();
}
//CI-6 CUSTOMIZATION End

//CI-6 CUSTOMIZATION - Set selected template name to hidden input selectedTemplate
function setSelectedTemplate() {
	var reportDropdown = document.getElementById("datatable-selector");
	var reportSelectedIndex = reportDropdown.selectedIndex;
	
	// Skip first selected item "Select :"
	if (reportSelectedIndex > 0) {
		var rptId = reportDropdown.options[reportSelectedIndex].value;
		var idx = rptId.slice(3);		// get the index for export-xlsx dropdown list
		var templateDropdown = document.querySelectorAll(".export-xlsx")[idx].getElementsByTagName("select")[0];
		var selectedTemplateName = templateDropdown.options[templateDropdown.selectedIndex].text;
		
		document.getElementById("selectedTemplate").value = selectedTemplateName;
	}
}
//CI-6 CUSTOMIZATION End
</script>